from django import forms

attrs = {
    'class': 'form-control'
    }
class postForm(forms.Form):
    nama = forms.CharField(required=True, max_length=100, widget=forms.TextInput(attrs=attrs))
    keadaan = forms.CharField(required=True, max_length=300, widget=forms.TextInput(attrs=attrs))