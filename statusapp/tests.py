from datetime import timezone

from django.test import TestCase, Client


# Create your tests here.
from django.urls import resolve
from django.utils import *
from .views import *
from .models import Post

class Week6Test(TestCase):

    def test_6_url(self):
        response = Client().get('/')
        self.assertEquals(response.status_code,200)

    def test_6asal(self):
        response = Client().get('/wkwkwkwkw')
        self.assertEquals(response.status_code, 404)

    def test_6_url_using_boxviews(self):
        found = resolve('/')
        self.assertEquals(found.func, posting)

    def test_lab_6_using_to_do_list_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'yeet.html')

    def test_model_can_create_new_activity(self):
        # Creating a new activity
        new_activity = Post.objects.create(nama="something", keadaan='Aku mau praktikum ngoding deh')
        # Retrieving all available activity
        counting_all_available_activity = Post.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

class ProfileTest(TestCase):

    def profile_page(self):
        response = Client().get("/profile/")
        self.assertEquals(response.status_code, 200)

    def test_template(self):
        response = Client().get("/profile/")
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "profile.html")

    def test_templatex(self):
        response = Client().get("/profile/")
        self.assertEquals(response.status_code, 200)
        self.assertTemplateNotUsed(response, "yeet.html")

    def test_html(self):
        response = Client().get('/profile/')
        self.assertContains(response, 'satrio')

    def test_html2(self):
        response = Client().get('/profile/')
        self.assertNotContains(response, 'fakerfakerplaymaker')


