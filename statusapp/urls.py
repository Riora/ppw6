from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views


urlpatterns = [
    path('', views.posting, name="yeet"),
    path('profile/', views.profile, name="profile"),
    ]