from django.http import HttpResponse
from django.shortcuts import render
from . import templates
from .models import Post
from .forms import postForm
from datetime import datetime
import pytz
import json

statuses={}

# Create your views here.


def profile(request):
    return render(request, 'profile.html')

def posting(request):
    response = {}
    response['forms'] = postForm()
    response['post'] = Post.objects.all()
    if (request.method == "POST"):
        nama = request.POST['nama']
        keadaan = request.POST['keadaan']
        waktu = datetime.now()
        post = Post(nama=nama, keadaan=keadaan, waktu=waktu)
        post.save()
    return render(request, 'yeet.html', response)

