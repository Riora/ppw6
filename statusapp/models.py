from datetime import timezone
from django.utils import *

from django.db import models

# Create your models here.
class Post(models.Model):
    object = models.Manager
    waktu = models.DateTimeField(default = timezone.now)
    nama = models.CharField(max_length=100)
    keadaan = models.CharField(max_length=300)

    def __str__(self):
        return self.keadaan